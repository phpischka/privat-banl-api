<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title></title>
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
  </head>
  <body>
    <div id="app">
      <base-component></base-component>
    </div>
    <script type="text/javascript" src="{{mix('js/app.js')}}"></script>
  </body>
</html>
