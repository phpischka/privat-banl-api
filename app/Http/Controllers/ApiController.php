<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax(Request $request)
    {
      $request_date = $request->date;
      $array_date = explode("-", $request_date);
      $day = $array_date[2];
      $month = $array_date[1];
      $year = $array_date[0];
      $date = $day . '.' . $month . '.' . $year;

      // create curl resource
      $ch = curl_init();

      // set url
      curl_setopt($ch, CURLOPT_URL, "https://api.privatbank.ua/p24api/exchange_rates?json&date=" . $date);

      //return the transfer as a string
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      // $output contains the output string
      $output = curl_exec($ch);

      return $output;
    }
}
